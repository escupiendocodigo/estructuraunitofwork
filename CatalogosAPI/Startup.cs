﻿using Autofac.Extensions.DependencyInjection;
using CosmosUnitOfWork.GlobalConfig;
using KuramaPersistencia.Enums;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace ClienteAPI
{
    public class Startup
    {
        #region [ Constructor ]
        /// <summary>
        /// Genera un nuevo objeto de configuración de la aplicación en tiempo de ejecución y lo construye.
        /// </summary>
        /// <param name="env">IHostingEnvironment: objeto que contiene la información del host en tiempo de ejecución.</param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        #endregion [ Constructor ]

        #region [ Propiedades ]
        /// <summary>
        /// Encapsula la configuración de propiedades de la aplicación.
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// Contenedor de la aplicación.
        /// Un contenedor es un paquete que permite utilizar una misma aplicación o componente de software en otro ambiente o S.O.
        /// </summary>
        public Autofac.IContainer ApplicationContainer { get; private set; }
        #endregion [ Propiedades ]

        #region [ Métodos de clase ]
        /// <summary>
        /// Agrega un servicio al contenedor de la aplicación. Es llamado en tiempo de ejecución.
        /// </summary>
        /// <param name="services">IServiceCollection: colección de servicios.</param>
        /// <returns>IServiceProvider: Objeto de servicio que encapsula al contenedor de la aplicación y proporciona 
        /// compatibilidad perzonalizada con otros objetos.</returns>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //Cadena conexion
            SharedConfig.SetConexion(KuramaAmbiente.Desarrollo);
            //Swagger
            AddSwagger(services);

            services.AddCors(o => o.AddPolicy("AllowSpecificOrigin", builder =>
            {
                builder.WithOrigins("*")
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddMvc();
            this.ApplicationContainer = ConfigInjection.InjectInstances(services);
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"COSMOS {groupName}",
                    Version = groupName,
                    Description = "COSMOS Catalogos API",
                    Contact = new OpenApiContact
                    {
                        Name = "Cosmos Company",
                        Email = string.Empty,
                        Url = new Uri("https://cosmos.com.mx/CatalogosAPI"),
                    }
                });
            });
        }
        /// <summary>
        /// Configura la tubería (pipeline) de la solicitud HTTP. Es llamado en tiempo de ejecución.
        /// </summary>
        /// <param name="app">IApplicationBuilder: provee la configuración un pipeline de una solicitud HTTP.</param>
        /// <returns>Void.</returns>
        public void Configure(IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cosmos CatalogosAPI V1");
            });
            app.UseCors("AllowSpecificOrigin");
            app.UseMvc();
        }

        #endregion [ Métodos de clase ]
    }
}
