﻿/*
  *Descripcion:  Este componente tiene la responsabilidad de proporcionar los métodos para el manejo de injección de dependencias
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */
#region [ Usings ]
using Autofac;
using Autofac.Extensions.DependencyInjection;
using CatalogosBusiness.CatalogosGenericos.Interface;
using Microsoft.Extensions.DependencyInjection;
#endregion

namespace AuthorizeAPI
{
    /// <summary>
    /// Este componente tiene la responsabilidad de proporcionar los métodos para el manejo de injección de dependencias
    /// </summary>
    public static class ConfigInjection
    {
        #region [ Atributos de clase ]
        /// <summary>
        /// Contenedor de aplicación, para iniciar el trabajo.
        /// </summary>
        private static IContainer ApplicationContainer;
        #endregion  [ Atributos de clase ]

        #region [ Métodos de clase ]
        /// <summary>
        /// Construye un contenedor de aplicación para agregarle una colección de servicios.
        /// </summary>
        /// <param name="services">IServiceCollection: colección de servicio a agregar al contenedor.</param>
        /// <returns>IContainer: contenedor de aplicación construido.</returns>
        public static IContainer InjectInstances(IServiceCollection services)
        {
            #region [ Inicializados del Contenedor de Instancias]
            var builder = new Autofac.ContainerBuilder();
            #endregion  [ Inicializados del Contenedor de Instancias ]

            #region [ Registro de Instancias Modificable ]
            //builder.RegisterType<PlazaService>().As<IPlazaService<Cat_Plazas>>().InstancePerDependency();

            #region  [ Factoria de catalogos ]
            #endregion  [ Factoria de catalogos ]


            #endregion  [ Registro de Instancias ]

            #region [ Inclusion y lanzamiento de Instancias]
            builder.Populate(services);
            return ApplicationContainer = builder.Build();
            #endregion [ Inclusion y lanzamiento de Instancias]
        }
        #endregion [ Métodos de clase ]
    }
}
