﻿/*
 * Descripción: Este componente se encarga definir los metodos del repositorio
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */
#region [ Usings ]
using CosmosUnitOfWork.Data;
using System.Linq;
#endregion

namespace CosmosUnitOfWork.GenericRepository
{
    /// <summary>
    ///  Este componente se encarga definir los metodos del repositorio
    /// </summary>
    /// <typeparam name="TEntity">TEntity clase generic del objeto</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        #region [ Descripciones ]
        /// <summary>
        /// Obtiene una entidad a partir de parámetros de búsqueda.
        /// </summary>
        /// <param name="keyValues">params object[]: parámetros de búsqueda.</param>
        /// <returns>TEntity: entidad obtenida.</returns>
        TEntity FirstOrDefault(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Obtiene la entidad asociada al objeto en forma de query.
        /// </summary>
        /// <returns>IQueryable(TEntity): entidad en forma de query.</returns>
        IQueryable<TEntity> Table(string sql, object param = null, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Inserta en la BD una entidad.
        /// </summary>
        /// <param name="entity">TEntity: entidad a insertar.</param>
        /// <returns>Void.</returns>
        void Insert(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Guarda los cambios dentro de la base de datos
        /// </summary>
        /// <returns></returns>
        void SaveChange(bool transaction = false);
        /// <summary>
        /// Elimina una entidad en la base de datos
        /// </summary>
        /// <param name="entity">TEntity: entidad a eliminar</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Método de consulta(Texto, Procedimiento)</param>
        void Delete(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Actualiza en la BD una entidad.
        /// </summary>
        /// <param name="entity">TEntity: entidad a actualizar.</param>
        /// <returns>Void.</returns>
        void Update(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        #endregion [ Descripciones ]
    }
}
