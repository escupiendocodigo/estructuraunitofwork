﻿/*
 * Descripción: Este componente se encarga implementar las acciones de los servicios dentro del contexto
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using CosmosUnitOfWork.Data;
using CosmosUnitOfWork.DataContext;
using CosmosUnitOfWork.Helpers;
using System.Linq;

#endregion

namespace CosmosUnitOfWork.GenericRepository
{
    /// <summary>
    /// Este componente se encarga implementar las acciones de los servicios dentro del contexto
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region [ Atributos de Clase ] 
        /// <summary>
        /// Contexto de base de datos
        /// </summary>
        private readonly IDBContext _context;
        /// <summary>
        /// Unidad de trabajo dentro del contexto 
        /// </summary>
        public readonly IUnitOfWork _uow;
        /// <summary>
        /// Gestor de conexión
        /// </summary>
        public readonly IConnectionFactory _connectionFactory;
        #endregion [ Atributos de Clase ]

        #region [ Constructor ]

        /// <summary>
        /// Inicializa los atributos de clase.
        /// </summary>
        /// <param name="conectionString"></param>
        /// <param name="provider"></param>
        public Repository()
        {
            _connectionFactory = IConnectionFactory.Create();
            _context = new DapperDBContext();
            _uow = new UnitOfWork(_connectionFactory);
        }
        #endregion 


        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de agregar los objetos para insertar
        /// </summary>
        /// <param name="entity">TEntity generica</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Tipo de método a realizar(Consulta o Procedimiento) </param>
        public void Insert(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento)
        {
            _context.Insert<TEntity>(entity, sql, method);
        }
        /// <summary>
        /// Obtiene un registro por default
        /// </summary>
        /// <param name="id">Idpara metro de busqueda</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Tipo de método a realizar(Consulta o Procedimiento) </param>
        /// <returns></returns>
        public TEntity FirstOrDefault(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento)
        {
            return _context.Table<TEntity>().FirstOrDefault(sql, entity, method);
        }
        /// <summary>
        /// Método encargado de regresar un listado de datos de tipo IQueryTable
        /// </summary>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="param">Parametros de busqueda</param>
        /// <param name="method">Tipo de método a realizar(Consulta o Procedimiento) </param>
        /// <returns></returns>
        public IQueryable<TEntity> Table(string sql, object param = null, InsertMethodEnum method = InsertMethodEnum.Procedimiento)
        {
            return _context.Table<TEntity>().Query(sql, param, method).AsQueryable();
        }
        /// <summary>
        /// Método encargado de realizar el commit 
        /// </summary>
        /// <returns></returns>
        public void SaveChange(bool transaction = false)
        {
            _context.SaveChange(transaction);
        }
        /// <summary>
        /// Método encargado de realizar la eliminación de un registro
        /// </summary>
        /// <param name="id">Id Parametro a eliminar</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Tipo de método a realizar(Consulta o Procedimiento) </param>
        public void Delete(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento)
        {
            _context.Delete<TEntity>(entity, sql, method);
        }
        /// <summary>
        /// Método encargado de realizar la actualización de una entidad de tipo TEntity
        /// </summary>
        /// <param name="entity">TEntity generica</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Tipo de método a realizar(Consulta o Procedimiento) </param>
        public void Update(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento)
        {
            _context.Update<TEntity>(entity, sql, method);
        }
        #endregion


    }
}
