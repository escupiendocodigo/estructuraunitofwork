﻿/*
 * Descripción: Este componente se encarga manejar el nombre de la cadena de conexión y métodos genericos dentro de toda la aplicación
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */
#region [ Usings ]
using KuramaPersistencia.Enums;
using System;
using System.IO;
#endregion

namespace CosmosUnitOfWork.GlobalConfig
{
    /// <summary>
    /// Descripción: Este componente se encarga manejar el nombre de la cadena de conexión y métodos genericos dentro de toda la aplicación
    /// </summary>
    public static class SharedConfig
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Propiedad encargada de seleccionar el nombre de la cadena de conexión para ambiente de preproductivo
        /// </summary>
        //public static int Ambiente { get; set; }
        /// <summary>
        /// Cadena de conexión de la aplicación
        /// </summary>
        public static string ConectionString { get; set; }
        /// <summary>
        /// Provider de base de datos
        /// </summary>
        public static string Provider { get; set; }
        /// <summary>
        /// Ruta de salida de archivo log
        /// </summary>
        public static string EnviromentLog { get; set; }
        #endregion

        static SharedConfig()
        {
            Provider = "System.Data.SqlClient";
            EnviromentLog = "C:\\tempLogs\\";
        }
        #region [ Métodos de Clase ]
        public static void SetConexion(KuramaAmbiente ambiente)
        {
            string cnn = string.Empty;
            switch (ambiente)
            {
                case KuramaAmbiente.Productivo:
                    cnn = "";
                    break;
                case KuramaAmbiente.Preproductivo:
                    cnn = "";
                    break;
                case KuramaAmbiente.Desarrollo:
                    cnn = "";
                    break;
            }
            ConectionString = cnn;
        }
        /// <summary>
        /// Método encargado de insertar una cadena en el logfile diario 
        /// </summary>
        /// <param name="mensaje">Descripción breve del mensaje</param>
        /// <param name="logCodes">Tipo de mensaje (Error,Mensaje,Success)</param>
        public static void Log(string mensaje, EventLogCodes logCodes = EventLogCodes.Mensaje)
        {
            string str = '\r' + ('\n' + (DateTime.Now + ($"----> {logCodes}" + '\n' + mensaje)));

            filelog(str);
        }
        /// <summary>
        /// Método encargado de crear el path y el archivo log.
        /// </summary>
        /// <param name="cadena">Mensaje a escribir en el archivo log.</param>
        private static void filelog(string cadena)
        {
            string path = EnviromentLog;//ConfigurationManager.AppSettings["enviroment"];
            try
            {
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }
                StreamWriter sw = new StreamWriter(Path.Combine(path, $"{DateTime.Now.Day}-{DateTime.Now.Month}-{DateTime.Now.Year}_Reporte.log"), true);
                sw.WriteLine(cadena);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
