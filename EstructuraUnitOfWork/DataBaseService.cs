﻿/*
* Descripción: Este componente se encarga manejar los servicios dentro de la base de datos
* Autor: Jorge Antonio Zepeda Ramírez
* Fecha de creación: 07/10/2020
* Documentación:
* 
* 
* Fecha de modificación:
* Autor de modificación:
* Descripción de modificación:
*/
#region [ Usings ]
using HaciendaChiapasUnitOfWork.Data;
using HaciendaChiapasUnitOfWork.Helpers;
#endregion


namespace HaciendaChiapasUnitOfWork
{
    /// <summary>
    ///  Descripción: Este componente se encarga manejar los servicios dentro de la base de datos
    /// </summary>
    public class DataBaseService : IDataBase
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Propiedad encargada del proveedor de base de datos
        /// </summary>
        public readonly IConnectionFactory _connectionFactory;
        /// <summary>
        /// Propiedad encargada de la unidad de trabajo
        /// </summary>
        public readonly IUnitOfWork _uow;
        #endregion

        #region [ Constructor de Clase ]
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionFactory"></param>
        /// <param name="uow"></param>
        public DataBaseService(IConnectionFactory connectionFactory, IUnitOfWork uow)
        {
            this._connectionFactory = connectionFactory;
            this._uow = uow;
        }
        #endregion

    }
}
