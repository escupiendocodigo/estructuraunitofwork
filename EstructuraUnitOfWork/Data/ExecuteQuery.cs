﻿/*
 * Descripción: Este componente se encarga de realizar las ejecuciones sql de la base de datos
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */
#region [ Usings ]
using CosmosUnitOfWork.Helpers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
#endregion

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga de realizar las ejecuciones sql de la base de datos
    /// </summary>
    public class ExecuteQuery : IExecuteQuery
    {
        #region [ Atributos de Clase ]
        /// <summary>
        ///  Propiedad encargada de realizar la instancia de la base de datos
        /// </summary>
        public readonly IConnectionFactory _connectionFactory;
        /// <summary>
        /// Propiedad encargada de contener la instancia de la base de datos
        /// </summary>
        public IDbConnection _connection;
        #endregion

        #region [ Constructor de Clase ]
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="connectionFactory"></param>
        public ExecuteQuery(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        #endregion

        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de realizar una consulta y devolver un IEnumerable de la clase génerica IEnumerable<TEntity>
        /// </summary>
        /// <typeparam name="TEntity">Entidad a mapear</typeparam>
        /// <param name="sql">Consulta sql</param>
        /// <param name="param">Parametros de busqueda</param>
        /// <param name="method">Método de busqueda (Text ó Procedimiento)</param>
        /// <returns></returns>
        public IEnumerable<TEntity> Query<TEntity>(string sql, object param, InsertMethodEnum method)
        {
            IEnumerable<TEntity> result = new List<TEntity>();
            try
            {
                _connection = _connectionFactory.CrearConexion();
                _connection.Open();
                switch (method)
                {
                    case InsertMethodEnum.Procedimiento:
                        if (param != null)
                        {
                            result = _connection.Query<TEntity>(sql, param, commandType: CommandType.StoredProcedure);
                        }
                        else
                        {
                            result = _connection.Query<TEntity>(sql, commandType: CommandType.StoredProcedure);
                        }
                        break;
                    case InsertMethodEnum.Text:
                        if (param != null)
                        {
                            result = _connection.Query<TEntity>(sql, param);
                        }
                        else
                        {
                            result = _connection.Query<TEntity>(sql);
                        }
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _connection.Close();
            }
            return result;
        }
        /// <summary>
        /// Método encargado de realizar una consulta y devolver una instancia de la clase génerica TEntity
        /// </summary>
        /// <typeparam name="TEntity">Entidad a mapear</typeparam>
        /// <param name="sql">Consulta sql</param>
        /// <param name="param">Parametros de busqueda</param>
        /// <param name="method">Método de busqueda (Text ó Procedimiento)</param>
        /// <returns></returns>
        public TEntity QuerySingle<TEntity>(string sql, object param, InsertMethodEnum method)
        {
            TEntity result = default;
            try
            {
                _connection = _connectionFactory.CrearConexion();
                _connection.Open();
                switch (method)
                {
                    case InsertMethodEnum.Procedimiento:
                        if (param != null)
                        {
                            result = _connection.Query<TEntity>(sql, param, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                        else
                        {
                            result = _connection.Query<TEntity>(sql, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                        break;
                    case InsertMethodEnum.Text:
                        if (param != null)
                        {
                            result = _connection.Query<TEntity>(sql, param).FirstOrDefault();
                        }
                        else
                        {
                            result = _connection.Query<TEntity>(sql).FirstOrDefault();
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("sql exception", ex);
            }
            finally
            {
                _connection.Close();
            }

            return result;
        }
        #endregion
    }
}
