﻿/*
 * Descripción: Este componente se encarga manejar los servicios de consulta
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using CosmosUnitOfWork.Helpers;
using System;
using System.Collections.Generic;

#endregion

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    ///  Descripción: Este componente se encarga manejar los servicios de consulta
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class QueryService<TEntity> : IQuery<TEntity>
    {
        #region [ Atributos de Clase ]
        QueryContext _queryContext;
        IConnectionFactory _connectionFactory;
        IExecuteQuery _executeQuery;
        #endregion

        #region [ Constructor de Clase ]
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionStringName"></param>
        public QueryService()
        {
            _connectionFactory = IConnectionFactory.Create();
            _executeQuery = new ExecuteQuery(_connectionFactory);
            _queryContext = new QueryContext(_executeQuery);
            _queryContext.EntityType = typeof(TEntity);
        }
        #endregion

        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de realizar la consulta de una clase generica de tipo TEntity
        /// </summary>
        /// <param name="sql">Consulta o Procedimiento sql</param>
        /// <param name="args">Argumentos para la busqueda</param>
        /// <param name="method">Método de consulta (Texto, Procedimiento)</param>
        /// <returns></returns>
        public TEntity FirstOrDefault(string sql, object args, InsertMethodEnum method)
        {
            var result = _executeQuery.QuerySingle<TEntity>(sql, args, method);
            return result;
        }
        /// <summary>
        /// Método encargado de realizar la consulta de una clase generica de tipo IEnumerable<TEntity>
        /// </summary>
        /// <param name="sql">Consulta o Procedimiento sql</param>
        /// <param name="args">Argumentos para la busqueda</param>
        /// <param name="method">Método de consulta (Texto, Procedimiento)</param>
        /// <returns></returns>
        IEnumerable<TEntity> IQuery<TEntity>.Query(string sql, object param, InsertMethodEnum method)
        {
            return _executeQuery.Query<TEntity>(sql, param, method);
        }
        #endregion
    }
}
