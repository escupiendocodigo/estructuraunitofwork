﻿/*
 * Descripción: Este componente se encarga de declarar los métodos para realizar un crud dentro de la unidad de trabajo
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
#endregion

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga de declarar los métodos para realizar un crud dentro de la unidad de trabajo
    /// </summary>
    public interface IUnitOfWork
    {
        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de agregar la instancia ala unidad de trabajo 
        /// </summary>
        /// <param name="sql">Consulta sql o Procedimiento</param>
        /// <param name="paramObject">Parametros</param>
        /// <param name="method">Método de consulta (Text, Procedimiento)</param>
        void Add(string sql, object paramObject, InsertMethodEnum method);
        /// <summary>
        /// /// Método que se encarga de realizar el commit dentro del UnitOfWork
        /// </summary>
        /// <param name="transaction">bool para identificar si se usa o no una transaction desde el código</param>
        void Commit(bool transaction = false);
        /// <summary>
        /// Método que se encarga de realizar la acción commit dentro del unit work
        /// </summary>
        /// <param name="transaction">bool para identificar si se usara o no una transferencia desde el código</param>
        /// <returns>int Id insertado</returns>
        int commit(bool transaction = false);
        #endregion

    }
}
