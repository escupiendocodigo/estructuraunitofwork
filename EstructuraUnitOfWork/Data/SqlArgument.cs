﻿/*
 * Descripción: Este componente se encarga manejar los argumentos para los métodos para la unidad de trabajo
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga manejar los argumentos para los métodos para la unidad de trabajo
    /// </summary>
    public class SqlArgument
    {

        #region [ Atributos de Clase ]
        /// <summary>
        /// Propiedad de consulta sql
        /// </summary>
        public string Sql { get; private set; }
        /// <summary>
        /// Propiedad de parametros 
        /// </summary>
        public object ParamObj { get; private set; }
        /// <summary>
        /// Propiedad Enum de tipo InsertMethodEnum
        /// </summary>
        public InsertMethodEnum InsertMethod { get; private set; }
        #endregion

        #region [ Constructor de Clase ]
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="paramObject">Parametros</param>
        /// <param name="method">Método de consulta (Texto o Procedimiento)</param>
        public SqlArgument(string sql, object paramObject, InsertMethodEnum method = InsertMethodEnum.Text)
        {
            Sql = sql;
            ParamObj = paramObject;
            InsertMethod = method;
        }
        #endregion

    }
}
