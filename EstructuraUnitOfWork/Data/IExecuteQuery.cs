﻿/*
 * Descripción: Este componente se encarga de declarar los métodos para realizar las ejecuciones sql de la base de datos
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]

#endregion

using System.Collections.Generic;

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga de declarar los métodos para realizar las ejecuciones sql de la base de datos
    /// </summary>
    public interface IExecuteQuery
    {
        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de realizar una consulta y devolver un IEnumerable de la clase génerica IEnumerable<TEntity>
        /// </summary>
        /// <typeparam name="TEntity">Entidad a mapear</typeparam>
        /// <param name="sql">Consulta sql</param>
        /// <param name="param">Parametros de busqueda</param>
        /// <param name="method">Método de busqueda (Text ó Procedimiento)</param>
        /// <returns></returns>
        IEnumerable<TEntity> Query<TEntity>(string sql, object param, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Método encargado de realizar una consulta y devolver una instancia de la clase génerica TEntity
        /// </summary>
        /// <typeparam name="TEntity">Entidad a mapear</typeparam>
        /// <param name="sql">Consulta sql</param>
        /// <param name="param">Parametros de busqueda</param>
        /// <param name="method">Método de busqueda (Text ó Procedimiento)</param>
        /// <returns></returns>
        TEntity QuerySingle<TEntity>(string sql, object param, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        #endregion
    }
}
