﻿/*
 * Descripción: Este componente se encarga de declarar los enums para los diferentes metodos de consulta sql
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga de declarar los enums para los diferentes metodos de consulta sql
    /// </summary>
    public enum InsertMethodEnum
    {
        #region [ Atributos de Enum ]
        /// <summary>
        /// Propiedad para espeficiar que la consulta es texto
        /// </summary>
        Text,
        /// <summary>
        /// Propiedad para especificar que la consulta se trata de un procedimiento almacenado
        /// </summary>
        Procedimiento
        #endregion
    }
}
