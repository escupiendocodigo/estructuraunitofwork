﻿/*
 * Descripción: Este componente se encarga manejar los servicios de la unidad de trabajo
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using CosmosUnitOfWork.Helpers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
#endregion

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga manejar los servicios de la unidad de trabajo
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Lista de acciones sql
        /// </summary>
        List<SqlArgument> _sqlList = new List<SqlArgument>();
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, object> _ParentKeyDic;
        /// <summary>
        /// Atributo para gestionar la factoria de la conexión
        /// </summary>
        IConnectionFactory _connectionFactory;
        #endregion

        #region [ Constructor de Clase ]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionFactory"></param>
        public UnitOfWork(IConnectionFactory connectionFactory)
        {
            _ParentKeyDic = new Dictionary<string, object>();
            _connectionFactory = connectionFactory;
        }
        #endregion

        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de realizar la agregación de los modelos de tipo SqlArgument dentro del UnitOfWork
        /// </summary>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="paramObject">Parametros</param>
        /// <param name="method">Método de inserción (Text, Procedimiento)</param>
        public void Add(string sql, object paramObject, InsertMethodEnum method)
        {
            SqlArgument model = new SqlArgument(sql, paramObject, method);
            _sqlList.Add(model);
        }
        /// <summary>
        /// /// Método que se encarga de realizar el commit dentro del UnitOfWork
        /// </summary>
        /// <param name="transaction">bool para identificar si se usa o no una transaction desde el código</param>
        public void Commit(bool transaction = false)
        {
            string executeSql = "";
            if (transaction)
            {
                using (IDbConnection conn = _connectionFactory.CrearConexion())
                {
                    conn.Open();
                    IDbTransaction tran = conn.BeginTransaction();
                    try
                    {
                        foreach (SqlArgument model in _sqlList)
                        {

                            executeSql = model.Sql;
                            switch (model.InsertMethod)
                            {
                                case InsertMethodEnum.Text:
                                    conn.Execute(model.Sql, model.ParamObj, tran);
                                    break;
                                default:
                                    conn.Execute(model.Sql, model.ParamObj, tran, commandType: CommandType.StoredProcedure);
                                    break;
                            }
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw new Exception(string.Format("sql excepción.sql={0} {1}.\r\n", executeSql,ex.Message), ex);
                    }
                    finally
                    {
                        _sqlList.Clear();
                        _ParentKeyDic.Clear();
                        conn.Close();
                    }
                }
            }
            else
            {
                using (IDbConnection conn = _connectionFactory.CrearConexion())
                {
                    conn.Open();
                    try
                    {
                        foreach (SqlArgument model in _sqlList)
                        {

                            executeSql = model.Sql;
                            switch (model.InsertMethod)
                            {
                                case InsertMethodEnum.Text:
                                    conn.Execute(model.Sql, model.ParamObj, null);
                                    break;
                                default:
                                    conn.Execute(model.Sql, model.ParamObj, null, commandType: CommandType.StoredProcedure);
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("sql excepción.sql={0} {1}.\r\n", executeSql, ex.Message), ex);
                    }
                    finally
                    {
                        _sqlList.Clear();
                        _ParentKeyDic.Clear();
                        conn.Close();
                    }
                }
            }
        }
        /// <summary>
        /// Método que se encarga de realizar la acción commit dentro del unit work
        /// </summary>
        /// <param name="transaction">bool para identificar si se usara o no una transferencia desde el código</param>
        /// <returns>int Id insertado</returns>
        public int commit(bool transaction = false)
        {
            string executeSql = "";
            int executeResult = 0;
            if (transaction)
            {
                using (IDbConnection conn = _connectionFactory.CrearConexion())
                {
                    conn.Open();
                    IDbTransaction tran = conn.BeginTransaction();
                    try
                    {
                        foreach (SqlArgument model in _sqlList)
                        {

                            executeSql = model.Sql;
                            switch (model.InsertMethod)
                            {
                                case InsertMethodEnum.Text:
                                    executeResult = conn.Execute(model.Sql, model.ParamObj, tran);
                                    break;
                                default:
                                    executeResult = conn.ExecuteScalar<int>(model.Sql, model.ParamObj, tran, commandType: CommandType.StoredProcedure);
                                    break;
                            }
                            if (executeResult < 0)
                            {
                                throw new Exception("excepción de sql: las filas son cero");
                            }
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw new Exception(string.Format("sql excepción.sql={0}.\r\n", executeSql), ex);
                    }
                    finally
                    {
                        _sqlList.Clear();
                        _ParentKeyDic.Clear();
                        conn.Close();
                    }
                }
            }
            else
            {
                using (IDbConnection conn = _connectionFactory.CrearConexion())
                {
                    conn.Open();
                    try
                    {
                        foreach (SqlArgument model in _sqlList)
                        {

                            executeSql = model.Sql;
                            switch (model.InsertMethod)
                            {
                                case InsertMethodEnum.Text:
                                    executeResult = conn.Execute(model.Sql, model.ParamObj, null);
                                    break;
                                default:
                                    executeResult = conn.ExecuteScalar<int>(model.Sql, model.ParamObj, null, commandType: CommandType.StoredProcedure);
                                    break;
                            }
                            if (executeResult < 0)
                            {
                                throw new Exception("excepción de sql: las filas son cero");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("sql excepción.sql={0}.\r\n", executeSql), ex);
                    }
                    finally
                    {
                        _sqlList.Clear();
                        _ParentKeyDic.Clear();
                        conn.Close();
                    }
                }
            }
            return executeResult;
        }
        #endregion
    }
}
