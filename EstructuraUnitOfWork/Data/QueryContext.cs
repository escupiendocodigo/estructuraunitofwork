﻿/*
 * Descripción: Este componente se encarga de declarar los las propiedades génericas para el uso de consultas
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using System;
#endregion

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga de declarar los las propiedades génericas para el uso de consultas
    /// </summary>
    public class QueryContext
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Propiedad para denominar el tipo de clase 
        /// </summary>
        public Type EntityType { get; set; }
        /// <summary>
        /// Propiedad para generar los diferntes médotos de consulta de tipo IExecuteQuery
        /// </summary>
        public IExecuteQuery Query { get; private set; }
        #endregion


        #region [ Constructor de Clase ]
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="query"></param>
        public QueryContext(IExecuteQuery query)
        {
            Query = query;
        }
        #endregion
    }
}
