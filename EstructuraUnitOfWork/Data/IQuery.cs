﻿/*
 * Descripción: Este componente se encarga de declarar los métodos para los diferentes formas de consulta sql
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */
#region [ Usings ]

#endregion


using System.Collections.Generic;

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga de declarar los métodos para los diferentes formas de consulta sql
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IQuery<TEntity>
    {
        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de regresar un registro del tipo TEntity
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="args"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        TEntity FirstOrDefault(string sql, object args = null, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Método encargado de regresar una lista del tipo IEnumerable<TEntity> 
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        IEnumerable<TEntity> Query(string sql, object param = null, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        #endregion
    }
}
