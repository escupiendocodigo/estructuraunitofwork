﻿/*
 * Descripción: Este componente se encarga manejar las conexiónes a bases de datos
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using CosmosUnitOfWork.Helpers;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
#endregion

namespace CosmosUnitOfWork.Data
{
    /// <summary>
    /// Descripción: Este componente se encarga manejar las conexiónes a bases de datos
    /// </summary>
    public class SqlServerFactory : IConnectionFactory
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Propiedad encargada de manejar la cadena de conexión especificada
        /// </summary>
        public readonly string _connectionStringName;
        #endregion


        #region [ Constructor de Clase ]
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionStringName">nombre de la cadena de conexión del archivo app.config</param>
        public SqlServerFactory(string connectionStringName)
        {
            _connectionStringName = connectionStringName;
        }
        #endregion

        #region [ Métodos de Clase ]
        /// <summary>
        /// Método encargado de realizar la conexión ala base de datos
        /// </summary>
        /// <returns></returns>
        public override IDbConnection CrearConexion()
        {
            return new SqlConnection(_connectionStringName);
        }
        #endregion
    }
}
