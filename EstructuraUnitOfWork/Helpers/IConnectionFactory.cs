﻿/*
 * Descripción: Este componente se encarga manejar los servicios de la fabrica de datos
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using KuramaPersistencia.Enums;
using CosmosUnitOfWork.Data;
using CosmosUnitOfWork.GlobalConfig;
using System.Data;
#endregion

namespace CosmosUnitOfWork.Helpers
{
    /// <summary>
    /// Descripción: Este componente se encarga manejar los servicios de la fabrica de datos
    /// </summary>
    public abstract class IConnectionFactory
    {
        #region [ Constructor de Clase ]
        /// <summary>
        /// /// Método para la seleccion hacia el provedor de servicio de base de datos
        /// </summary>
        /// <param name="connectionStringName"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static IConnectionFactory Create()
        {
            IConnectionFactory factory = null;
            switch (SharedConfig.Provider)
            {
                case "System.Data.SqlClient":
                    factory = new SqlServerFactory(SharedConfig.ConectionString);
                    break;
                case "MySql.Data.MySqlClient":
                    factory = new MySqlFactory(SharedConfig.ConectionString);
                    break;
                default:
                    factory = new SqlServerFactory(SharedConfig.ConectionString);
                    break;
            }
            return factory;
        }
        #endregion
        #region [ Métodos de Clase]
        /// <summary>
        /// Método para crear la conexión del provedor de servicios
        /// </summary>
        /// <returns></returns>
        public abstract IDbConnection CrearConexion();
        #endregion
    }
}
