﻿/*
 * Descripción: Este componente se encarga declarar los servicios del contexto predeterminado
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using CosmosUnitOfWork.Data;
using System;
using System.Threading.Tasks;
#endregion

namespace CosmosUnitOfWork.DataContext
{
    /// <summary>
    /// Descripción: Este componente se encarga declarar los servicios del contexto predeterminado
    /// </summary>
    public interface IDBContext
    {
        #region [ Métodos de Clase ]
        /// <summary>
        /// Método para guardar un objeto de tipo TEntity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="sql"></param>
        /// <param name="method"></param>
        void Insert<TEntity>(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento) where TEntity : class;
        /// <summary>
        /// Método para actualizar o modificar algun objeto de tipo TEntity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="sql"></param>
        /// <param name="method"></param>
        void Update<TEntity>(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento) where TEntity : class;
        /// <summary>
        /// Método para Eliminar logicamente un objeto de tipo TEntity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="id"></param>
        /// <param name="sql"></param>
        /// <param name="method"></param>
        void Delete<TEntity>(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento);
        /// <summary>
        /// Método para guardar los cambios dentro del contexto
        /// </summary>
        /// <returns></returns>
        void SaveChange(bool transaction = false);
        /// <summary>
        /// Método encargado de regresar una tabla de tipo  IQuery<TEntity>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IQuery<TEntity> Table<TEntity>() where TEntity : class;
        #endregion
    }
}