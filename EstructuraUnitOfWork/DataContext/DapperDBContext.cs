﻿/*
 * Descripción: Este componente se encarga manejar los servicios del contexto predeterminado
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

#region [ Usings ]
using CosmosUnitOfWork.Data;
using CosmosUnitOfWork.GlobalConfig;
using CosmosUnitOfWork.Helpers;
#endregion

namespace CosmosUnitOfWork.DataContext
{

    /// <summary>
    /// Descripción: Este componente se encarga manejar los servicios del contexto predeterminado
    /// </summary>
    public class DapperDBContext : IDBContext
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Propiedad para definir los métodos para la unidad de trabajo
        /// </summary>
        public readonly IUnitOfWork _uow;
        /// <summary>
        /// Propiedad para definir las conexiónes para las bases de datos
        /// </summary>
        public readonly IConnectionFactory _connectionFactory;
        /// <summary>
        /// Propiedad para definir el proveedor de base de datos
        /// </summary>
        public string _provider;
        /// <summary>
        /// Propiedad para definir la cadena de conexión
        /// </summary>
        public string _connString;

        #endregion

        #region [ Constructor de Clase ]
        /// <summary>
        /// Contructor de Contexto
        /// </summary>
        /// <param name="connectionStringName">Cadena de conexión</param>
        /// <param name="provider">Proveedor de Base de datos</param>
        public DapperDBContext()
        {
            _connectionFactory = IConnectionFactory.Create();
            _connString = SharedConfig.ConectionString;
            _provider = SharedConfig.Provider;
            _uow = new UnitOfWork(_connectionFactory);
        }

        #endregion


        #region [ Métodos de Clase ]
        /// <summary>
        /// Método para guardar un objeto ala base de datos de tipo TEntity
        /// </summary>
        /// <typeparam name="TEntity">Entidad a guardar</typeparam>
        /// <param name="entity">Entidad a guardar</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Metodo de inserción (Texto, Procedimiento)</param>
        public void Insert<TEntity>(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento) where TEntity : class
        {
            _uow.Add(sql, entity, method);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity">Entidad a guardar</typeparam>
        /// <param name="entity">Entidad a guardar</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Metodo de actualización (Texto, Procedimiento)</param>
        public void Update<TEntity>(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento) where TEntity : class
        {
            _uow.Add(sql, entity, method);
        }

        /// <summary>
        /// Método para guardar los cambios dentro del UnitOfWork
        /// </summary>
        /// <returns></returns>
        public void SaveChange(bool transaction = false)
        {
            _uow.Commit(transaction);
        }

        /// <summary>
        /// Método encargado de realizar la consulta de tipo IQuery<TEntity>
        /// </summary>
        /// <typeparam name="TEntity">Entidad a consultar</typeparam>
        /// <returns></returns>
        public IQuery<TEntity> Table<TEntity>() where TEntity : class
        {
            return new QueryService<TEntity>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity">Entidad a guardar</typeparam>
        /// <param name="entity">Entidad a guardar</param>
        /// <param name="sql">Consulta o Procedimiento</param>
        /// <param name="method">Metodo de eliminación (Texto, Procedimiento)</param>
        public void Delete<TEntity>(object entity, string sql, InsertMethodEnum method = InsertMethodEnum.Procedimiento)
        {
            _uow.Add(sql, entity, method);
        }
        #endregion
    }
}
