﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KuramaPersistencia.Enums
{
    public enum KuramaAmbiente
    {

        #region [ Atributos de Clase ]
        /// <summary>
        /// Cadena de conexión de Productivo
        /// </summary>
        Productivo =0,
        /// <summary>
        /// Cadena de conexión de Preproductivo
        /// </summary>
        Preproductivo=1,
        /// <summary>
        /// Cadena de conexión de Desarrollo
        /// </summary>
        Desarrollo =2
        #endregion [ Atributos de Clase ]
    }
}
