﻿/* 
 *Descripción: Este componente tiene la responsabilidad de proporcionar un Enumerador de códigos de eventos y excepciones.
 * Autor: Jorge Antonio Zepeda Ramírez
 * Fecha de creación: 07/10/2020
 * Documentación:
 * 
 * 
 * Fecha de modificación:
 * Autor de modificación:
 * Descripción de modificación:
 */

namespace KuramaPersistencia.Enums
{
    /// <summary>
    /// Este componente tiene la responsabilidad de proporcionar un Enumerador de códigos de eventos y excepciones.
    /// </summary>
    public enum EventLogCodes
    {
        #region [ Atributos de Clase ]
        /// <summary>
        /// Trabajo exitoso
        /// </summary>
        Success,
        /// <summary>
        /// informó un error
        /// </summary>
        Error,
        /// <summary>
        /// Informa una cadena simple de mensaje
        /// </summary>
        Mensaje
        #endregion [ Atributos de Clase ]
    }
}
