﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KuramaPersistencia.GenericKuramaResponse
{
    public class GenericKuramaResponse<TGeneric>
    {
        public bool redireccionar { get; set; }
        public bool exito { get; set; }
        public string msj { get; set; }
        public TGeneric datos { get; set; }

        public GenericKuramaResponse()
        {
            redireccionar = false;
            exito = false;
            msj = string.Empty;
        }
    }
}
