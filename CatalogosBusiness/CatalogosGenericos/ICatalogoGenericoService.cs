﻿using KuramaPersistencia.GenericKuramaResponse;
using System.Collections.Generic;

namespace CatalogosBusiness.CatalogosGenericos.Interface
{
    public interface ICatalogoGenericoService<TClassCatalogoGenerico, TViewModelCatalogoGenerico>
    where TClassCatalogoGenerico : class
    where TViewModelCatalogoGenerico : class
    {
        #region [ Especificaciones ]
        /// <summary>
        /// Especificación del método que permite realizar la inserción de una clase Generica.
        /// </summary>
        /// <param name="TViewModelCatalogoGenerico">TViewModelCatalogoGenerico Entidad a guardar</param>
        /// <returns>Id insertado</returns>
        GenericKuramaResponse<int> Agregar(TViewModelCatalogoGenerico viewModelCatalogoGenerico);
        /// <summary>
        /// Especificación del método que permite realizar la actualización de una entidad Generica.
        /// </summary>
        /// <param name = "TViewModelCatalogoGenerico" ></ param >
        /// < returns >TViewModelCatalogoGenerico Entidad a actualizar</ returns >
        GenericKuramaResponse<int> Actualizar(TViewModelCatalogoGenerico viewModelCatalogoGenerico);
        ///// <summary>
        ///// Especificación del método que permite realizar la eliminacióon de una entidad generica.
        ///// </summary>
        ///// <param name = "TViewModelCatalogoGenerico"></ param >
        ///// < returns >bool respuesta de eliminación exitosa</ returns >
        GenericKuramaResponse<bool> Eliminar(int id);
        /// <summary>
        /// Especificación del método que permite realizar la consulta de una entidad generica.
        /// </summary>
        /// <param name="id">id generico a consultar</param>
        /// <returns>TClassCatalogoGenerico Entidad a consultar</returns>
        GenericKuramaResponse<TClassCatalogoGenerico> ObtenerById(int id);
        /// <summary>
        /// Especificación del método que permite realizar la consulta de todos los registros de la entidad generica.
        /// </summary>
        /// <returns>List<TClassCatalogoGenerico> lista generica consultada</returns>
        GenericKuramaResponse<List<TClassCatalogoGenerico>> Obtener(TViewModelCatalogoGenerico viewModelCatalogoGenerico);
        #endregion [ Especificaciones ] 
    }
}
