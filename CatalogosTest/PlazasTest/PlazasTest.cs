﻿//using EstructuraPersistencia.Enums;
//using CosmosUnitOfWork.DataContext;
//using CosmosUnitOfWork.GlobalConfig;
//using Dapper;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
//using System.Data;

//namespace Clientes.Test.Clientes.Test
//{
//    [TestClass]
//    public class ClienteTest_SP
//    {
//        IDBContext _db;
//        Random rng = new Random();
//        string[] nombres = new[]
//            {
//                "Chucho Negro", "Humberto", "Francisco", "Romeo", "MilBelis", "Alejandro", "Ricardo", "Eddy", "Francisco 2", "Marcos", "Juan", "Alee"
//            };

//        [TestInitialize]
//        public void Init()
//        {
//            SharedConfig.SetConexion(AmbienteCosmos.Desarrollo);
//            _db = new DapperDBContext();
//        }
//        [TestMethod]
//        public void Test_Insertar_Plaza()
//        {
//            CatPlazaViewModel model = new CatPlazaViewModel();
//            var param = new DynamicParameters();
//            for (int i = 127; i < 130; i++)
//            {
//                SharedConfig.Log($"{i} iteración", EventLogCodes.Success);
//                SharedConfig.Log($"{i} iteración", EventLogCodes.Mensaje);
//                SharedConfig.Log($"{i} iteración", EventLogCodes.Error);
//                param.Add("@Plaza", nombres[rng.Next(nombres.Length)]);
//                param.Add("@Existe", true);
//                param.Add("@Direccion", nombres[rng.Next(nombres.Length)]);
//                param.Add("@Id_Valida", 0);
//                param.Add("@EsCrea", true);
//                param.Add("@EsFinanciamiento", true);
//                param.Add("@EsProyectoExterno", true);
//                param.Add("@EsProyectoExterno_IdEmpresa", 0);
//                param.Add("@PapeleriaInsumo", 0);
//                param.Add("@PlazaLiberada", true);
//                param.Add("@PresupuestoCompras", true);
//                param.Add("@Creo", 0);
//                param.Add("@Modifico", 0);
//                param.Add("@Id_plaza_padre", 0);
//                param.Add("@Fecha_creo", DateTime.Now);
//                param.Add("@Activo", true);
//                param.Add("@Fecha_modifico", DateTime.Now);
//                param.Add("@Resultado", null, DbType.Int32, ParameterDirection.Output);
//                _db.Insert<CatPlazaViewModel>(model, "WEBCatalogoCat_PlazasInsertar");
//            }
//            _db.SaveChange();
//            Int32 resultado= param.Get<Int32>("@Resultado");
//            //Obtener el resultado que se guardo success
//            var respuesta = resultado == 0 ? true: false;
//            Assert.AreEqual(respuesta, true, "La inserción fue realizada con éxito");
//        }
       
//        [TestMethod]
//        public void Test_Actualizar_Plaza()
//        {
//            CatPlazaViewModel model = new CatPlazaViewModel();
//            var param = new DynamicParameters();
//            for (int i = 127; i < 130; i++)
//            {
//                SharedConfig.Log($"{i} iteración", EventLogCodes.Success);
//                SharedConfig.Log($"{i} iteración", EventLogCodes.Mensaje);
//                SharedConfig.Log($"{i} iteración", EventLogCodes.Error);
//                param.Add("@Id_Plaza", i);
//                param.Add("@Plaza", nombres[rng.Next(nombres.Length)]);
//                param.Add("@Existe", model.existe);
//                param.Add("@Direccion", model.direccion);
//                param.Add("@Id_Valida", model.idValida);
//                param.Add("@EsCrea", model.esCrea);
//                param.Add("@EsFinanciamiento", model.esFinanciamiento);
//                param.Add("@EsProyectoExterno", model.esProyectoExterno);
//                param.Add("@EsProyectoExterno_IdEmpresa", model.esProyectoExternoIdEmpresa);
//                param.Add("@PapeleriaInsumo", model.papeleriaInsumo);
//                param.Add("@PlazaLiberada", model.plazaLiberada);
//                param.Add("@PresupuestoCompras", model.presupuestoCompras);
//                param.Add("@Creo", model.creo);
//                param.Add("@Modifico", model.modifico);
//                param.Add("@Id_plaza_padre", model.idPlaza);
//                param.Add("@Fecha_creo", model.fechaCreo);
//                param.Add("@Activo", model.activo);
//                param.Add("@Fecha_modifico", model.fechaModifico);
//                param.Add("@Resultado", null, DbType.Int32, ParameterDirection.Output);
//                _db.Update<Cat_Plazas>(model, "WEBCatalogoCat_PlazasUpdate");
//            }
//            _db.SaveChange();
//            Int32 resultado = param.Get<Int32>("@Resultado");
//            //Obtener el resultado que se guardo success
//            var respuesta = resultado == 0 ? true : false;
//            Assert.AreEqual(respuesta, true, "La actualización fue realizada con éxito");
//        }
//        [TestMethod]
//        public void Test_Plaza_Inctivo()
//        {
//            var param = new DynamicParameters();
//            param.Add("@Id", 129);
//            param.Add("@Resultado", null, DbType.Int32, ParameterDirection.Output);

//            _db.Delete<Cat_Plazas>(param, "WEBCatalogoCat_PlazasInactiva");
//            _db.SaveChange();

//            Int32 resultado = param.Get<Int32>("@Resultado");
//            //Obtener el resultado que se guardo success
//            var respuesta = resultado == 0 ? true : false;
//            Assert.AreEqual(respuesta, true, "La actualización fue realizada con éxito");
//        }
//        [TestMethod]
//        public void Test_Plaza_GetIdPlaza()
//        {
//            var param = new DynamicParameters();
//            param.Add("@Id", 129);
//            var model = _db.Table<Cat_Plazas>().FirstOrDefault("WEBCatalogoCat_PlazasGetIdPlaza", param);

//            var respuesta = model != null ? true : false;
//            Assert.AreEqual(respuesta, true, "La actualización fue realizada con éxito");
//        }
//    }

//}
